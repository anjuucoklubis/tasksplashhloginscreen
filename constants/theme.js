import { Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");

export const COLORS = {
  black: "#3C3C3C",
  gray: "#d3d3d3",
  white: "#FFFFFF",
  red: "#EE4B2B",
  purple: "#663399",
};

const appTheme = { COLORS };

export default appTheme;
