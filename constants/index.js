import images from "./images";
import theme, { COLORS } from "./theme";

export { theme, images, COLORS };
