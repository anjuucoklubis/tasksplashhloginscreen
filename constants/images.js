const footerSplash = require("../assets/images/footer-splash.png");
const headerLogin = require("../assets/images/header-login.png");
const headerSplash = require("../assets/images/header-splash.png");
const logo = require("../assets/images/logo.png");

export default {
  footerSplash,
  headerLogin,
  headerSplash,
  logo,
};
