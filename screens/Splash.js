import { View, Image } from "react-native";
import React, { useEffect } from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { COLORS } from "../constants/theme";
import { images } from "../constants";

const Splash = ({ navigation }) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace("Login");
    }, 2000);
  }, [navigation]);

  return (
    <SafeAreaView style={{ backgroundColor: COLORS.white, flex: 1 }}>
      <View style={{ justifyContent: "center", alignItems: "center", flex: 1 }}>
        <View style={{ position: "absolute", top: -30, right: 0 }}>
          <Image
            source={images.headerSplash}
            style={{
              resizeMode: "contain",
              width: 340,
              height: 290,
            }}
          />
        </View>

        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            marginTop: 250,
          }}
        >
          <Image
            source={images.logo}
            style={{
              resizeMode: "contain",
              maxWidth: "80%",
              maxHeight: "80%",
            }}
          />
        </View>

        <View style={{}}>
          <Image
            source={images.footerSplash}
            style={{
              resizeMode: "contain",
              maxWidth: "100%",
              maxHeight: "100%",
            }}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Splash;
