import React, { useState } from "react";
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Modal,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { COLORS } from "../constants/theme";
import { images } from "../constants";
import { Ionicons } from "@expo/vector-icons";

const Login = () => {
  const [isPasswordShown, setIsPasswordShown] = useState(false);
  const [userID, setUserID] = useState("");
  const [password, setPassword] = useState("");
  const [showPopup, setShowPopup] = useState(false);
  const [popupMessage, setPopupMessage] = useState("");

  const handleLogin = () => {
    if (userID === "" || password === "") {
      setPopupMessage("User ID dan atau Password anda belum diisi.");
    } else {
      setPopupMessage("Login Berhasil");
    }
    setShowPopup(true);
  };

  const handlePopupOk = () => {
    setShowPopup(false);
    setPopupMessage("");
  };

  return (
    <SafeAreaView style={{ backgroundColor: COLORS.white, flex: 1 }}>
      <View style={{ flexDirection: "row" }}>
        <View>
          <Image source={images.headerLogin} />
        </View>
        <View style={{}}>
          <Image
            source={images.logo}
            style={{
              resizeMode: "contain",
              width: 120,
              height: 170,
            }}
          />
        </View>
      </View>

      <View
        style={{
          marginHorizontal: 22,
          marginTop: 75,
          alignItems: "center",
          marginLeft: 20,
        }}
      >
        <View style={{ justifyContent: "center" }}>
          <View>
            <Text style={{ fontSize: 25, fontWeight: "bold" }}>Login</Text>
            <Text style={{}}>Please sign in to continue.</Text>
          </View>

          <View
            style={{
              marginTop: 15,
            }}
          >
            <View style={{ marginBottom: 12 }}>
              <Text
                style={{ fontSize: 16, fontWeight: "400", marginVertical: 8 }}
              >
                User ID
              </Text>
              <View
                style={{
                  width: "100%",
                  height: 30,
                  borderColor: COLORS.black,
                  borderBottomWidth: 1,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <TextInput
                  style={{ width: "100%", fontStyle: "italic" }}
                  placeholder="User ID"
                  placeholderTextColor={COLORS.gray}
                  keyboardType="default"
                  value={userID}
                  onChangeText={(text) => setUserID(text)}
                />
              </View>
            </View>

            <View style={{ marginBottom: 12 }}>
              <Text
                style={{ fontSize: 16, fontWeight: "400", marginVertical: 8 }}
              >
                Password
              </Text>
              <View
                style={{
                  width: "100%",
                  height: 30,
                  borderColor: COLORS.black,
                  borderBottomWidth: 1,
                  alignItems: "flex-start",
                  paddingRight: 70,
                  justifyContent: "center",
                }}
              >
                <TextInput
                  style={{
                    width: "100%",
                    fontStyle: "italic",
                  }}
                  placeholder="Password"
                  placeholderTextColor={COLORS.gray}
                  secureTextEntry={!isPasswordShown}
                  value={password}
                  onChangeText={(text) => setPassword(text)}
                />

                <TouchableOpacity
                  onPress={() => setIsPasswordShown(!isPasswordShown)}
                  style={{ position: "absolute", right: 12 }}
                >
                  {isPasswordShown ? (
                    <Ionicons name="eye-off" size={24} color={COLORS.black} />
                  ) : (
                    <Ionicons name="eye" size={24} color={COLORS.black} />
                  )}
                </TouchableOpacity>
              </View>
            </View>

            <View>
              <TouchableOpacity onPress={handleLogin}>
                <Text
                  style={{
                    backgroundColor: COLORS.purple,
                    fontSize: 16,
                    fontWeight: "bold",
                    color: COLORS.white,
                    borderRadius: 20,
                    paddingHorizontal: 6,
                    paddingVertical: 10,
                    width: 120,
                    textAlign: "center",
                    marginLeft: 200,
                  }}
                >
                  LOGIN
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            bottom: -130,
          }}
        >
          <View>
            <Text>Don't have an account? </Text>
          </View>
          <View>
            <Text style={{ color: COLORS.red }}>Sign Up</Text>
          </View>
        </View>
      </View>

      {/* Popup */}
      <Modal transparent visible={showPopup}>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0, 0, 0, 0.5)",
          }}
        >
          <View
            style={{
              backgroundColor: COLORS.white,
              padding: 20,
              borderRadius: 8,
            }}
          >
            <Text
              style={{ fontSize: 16, fontWeight: "bold", marginBottom: 10 }}
            >
              {popupMessage}
            </Text>
            <TouchableOpacity
              onPress={handlePopupOk}
              style={{ alignSelf: "flex-end" }}
            >
              <Text style={{ color: COLORS.red, fontWeight: "bold" }}>OK</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

export default Login;
